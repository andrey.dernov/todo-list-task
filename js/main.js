const btnAdd = document.querySelector('.js-add-button');

btnAdd.addEventListener(
    'click',
    () => {
        const inputEl = getToDoInput();
        const inputValue = inputEl.value;
        inputEl.value = '';
        console.log(inputValue);
        if (inputValue === undefined || inputValue.length === 0) return;

        const list = getToDoList();
        if (!btnAdd.classList.contains('js-update-button')) {
            const newLi = getItemEl(inputValue, false);
            list.append(newLi);
        } else {
            const todoElementToUpdate = list.querySelector('.list-group-item--active');
            if (todoElementToUpdate) {
                updateElementText(todoElementToUpdate, inputValue)
                todoElementToUpdate.classList.remove('list-group-item--active');
            }
            btnAdd.textContent = 'Добавить';
            btnAdd.classList.remove('js-update-button');
        }

        saveToDoListToLocalStorage(list);
    }
);

document.querySelectorAll('.list-group-item').forEach(todoListItem => {
    const todoItemLabel = todoListItem.querySelector('.js-item-content');
    const todoBtnRemove = todoListItem.querySelector('.js-remove');
    const todoBtnEdit = todoListItem.querySelector('.js-edit');
    setListeners(todoItemLabel, todoBtnRemove, todoListItem, todoBtnEdit)
})

function getItemEl(text, isComplete) {
    const liEl = document.createElement('li');
    liEl.classList.add('list-group-item');
    const formCheckEl = document.createElement('div');
    formCheckEl.classList.add('form-check');

    const checkboxLabel = document.createElement('label');
    checkboxLabel.classList.add('js-item-content');

    //todo: generate unique label id
    const checkBoxId = `js-checkbox-${Math.random()}`
    checkboxLabel.setAttribute('for', checkBoxId);

    const checkBoxInputEl = document.createElement('input');
    checkBoxInputEl.classList.add('form-check-input');
    checkBoxInputEl.classList.add('js-checkbox');
    checkBoxInputEl.setAttribute('type', 'checkbox');
    checkBoxInputEl.setAttribute('id', checkBoxId);

    if (isComplete) {
        checkboxLabel.classList.add('done');
        checkBoxInputEl.checked = true
    }

    checkboxLabel.textContent = text;

    const editBtnEl = document.createElement('button');
    editBtnEl.classList.add('button__edit');
    editBtnEl.classList.add('js-edit');

    const removeBtnEl = document.createElement('button');
    removeBtnEl.classList.add('button__remove');
    removeBtnEl.classList.add('js-remove');
    removeBtnEl.textContent = 'x';

    formCheckEl.append(checkBoxInputEl);
    formCheckEl.append(checkboxLabel);

    liEl.append(formCheckEl);
    liEl.append(editBtnEl);
    liEl.append(removeBtnEl);
    setListeners(checkboxLabel, removeBtnEl, liEl, editBtnEl)
    return liEl;
}

function updateElementText(todoElement, text) {
    const todoText = todoElement.querySelector('.js-item-content');
    if (todoText) {
        todoText.textContent = text;
    }
}

function setListeners(todoItemLabel, todoBtnRemove, todoListItem, todoBtnEdit) {
    const toggleDoneListener = (event) => {
        event.target.classList.toggle('done');
        const toDoList = getToDoList();
        saveToDoListToLocalStorage(toDoList);
    }
    const editListener = () => {
        const mainInput = getToDoInput();
        const addButton = getToDoAddButton();
        const toDoList = getToDoList();
        const currentlyEditedTodo = toDoList.querySelector('.list-group-item--active');
        if (currentlyEditedTodo && currentlyEditedTodo !== todoListItem) {
            currentlyEditedTodo.classList.remove('list-group-item--active');
        } else {
            addButton.classList.toggle('js-update-button')
        }

        if (addButton.classList.contains('js-update-button')) {
            addButton.textContent = 'Обновить';
            mainInput.value = todoItemLabel.textContent.trim();
        } else {
            addButton.textContent = 'Добавить';
            mainInput.value = '';
        }

        todoListItem.classList.toggle('list-group-item--active')
    }
    const removeListener = () => todoListItem.remove();


    todoItemLabel.addEventListener('click', toggleDoneListener);
    todoBtnEdit.addEventListener('click', editListener);
    todoBtnRemove.addEventListener('click', () => {
        const toDoList = getToDoList();
        const currentlyEditedTodo = toDoList.querySelector('.list-group-item--active');
        if (currentlyEditedTodo && currentlyEditedTodo === todoListItem) {
            const addButton = getToDoAddButton();
            const mainInput = getToDoInput();
            addButton.classList.remove('js-update-button')
            addButton.textContent = 'Добавить';
            mainInput.value = '';
        }
        todoItemLabel.removeEventListener('click', toggleDoneListener);
        todoBtnEdit.removeEventListener('click', editListener);
        todoBtnRemove.removeEventListener('click', removeListener);
        todoListItem.remove();
        saveToDoListToLocalStorage(toDoList);
    });
}

function loadFromStorage() {
    const todoListString = getOrInitTodoList();
    const savedList = JSON.parse(todoListString);
    const list = getToDoList();
    savedList.items.forEach(i => {
            const newLi = getItemEl(i.text, i.isComplete);
            list.append(newLi);
        }
    )

}

function saveToDoListToLocalStorage(list) {
    const listToSave = {items: []}
    Array.from(list.children).forEach(li => {
        const todoText = li.querySelector('.js-item-content');
        const todoCheck = li.querySelector('.js-checkbox');
        const checkFor = todoCheck.getAttribute('id');
        const isDefaultCheck = checkFor && checkFor === 'flexCheckDefault'
        if (!isDefaultCheck && todoText) {
            listToSave.items.push({text: todoText.textContent, isComplete: todoText.classList.contains('done')});
        }
    })
    saveToDoList(listToSave);
}

function getToDoInput() {
    return document.querySelector('.js-input');
}

function getToDoList() {
    return document.querySelector('.js-list');
}

function getOrInitTodoList() {
    let todoListString = localStorage.getItem('todo-list');
    if (!todoListString) {
        localStorage.setItem('todo-list', JSON.stringify({items: []}));
        todoListString = localStorage.getItem('todo-list');
    }
    return todoListString;
}

function saveToDoList(todoListObject) {
    return localStorage.setItem('todo-list', JSON.stringify(todoListObject));
}

function getToDoAddButton() {
    return document.querySelector('.js-add-button');
}